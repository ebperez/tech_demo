<!DOCTYPE html>
<html lang="en">
<head>
  <title>Tasks</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    
    <h2>Tasks</h2>
    
    <form method="POST" action="/tasks">
        {{ csrf_field() }}
        <div class="form-group">
            <input type="text" class="form-control" id="description" placeholder="New Item" name="description" required>
        </div>
    </form>
    
    <table class="table">
        <thead>
        <tr>
            <th>To Do</th>
            <th>Status</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($tasks as $task)
        <tr>
            <td>{{ $task->description }}</td>
            
            @if ($task->completed == 1)
                <td>Complete</td>
                <td>
                <a href="/tasks/incomplete/{{ $task->id}}" class="btn btn-warning btn-xs">Mark Incomplete</a>
                </td>
            @else
                <td>Incomplete</td>
                <td>
                <a href="/tasks/complete/{{ $task->id}}" class="btn btn-success btn-xs">Mark Complete</a>
                </td>
            @endif
            <td>
                <!-- THIS SHOULD MARK A TASK AS DELETED -->
                <a href="/tasks/delete/{{ $task->id}}" class="btn btn-danger btn-xs">Delete Task</button>
            </td>
        </tr>
        @endforeach
        </tbody>
    </table>

</div>

<div class="container">
    <a href="/deleted" class="btn btn-info" role="button">View Deleted Tasks</a>
</div>

</body>
</html>
