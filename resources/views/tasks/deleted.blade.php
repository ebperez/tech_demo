<!DOCTYPE html>
<html lang="en">
<head>
  <title>Deleted Tasks</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Deleted Tasks</h2>
  <table class="table">
    <thead>
      <tr>
        <th>To Do</th>
        <th>Status</th>
        <th></th>
      </tr>
    </thead>
    <tbody>

    @foreach ($tasks as $task)
      
      <tr>
        <td>{{ $task->description }}</td>
        
        @if ($task->completed == 1)
            <td>Complete</td>
        
        @else
            <td>Incomplete</td>

        @endif

        <td>
            <a href="/tasks/restore/{{ $task->id}}" class="btn btn-danger btn-xs">Restore Task</button>
        </td>
      </tr>

    @endforeach
    
    </tbody>
  </table>
</div>

<div class="container">
    <a href="/tasks" class="btn btn-info" role="button">View Active Tasks</a>
</div>

</body>
</html>
