<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/tasks', function () {

    $tasks = App\Task::where('deleted', 0)->get();

    return view('tasks.index', compact('tasks'));

});

Route::get('/deleted', function () {

    $tasks = App\Task::where('deleted', 1)->get();

    return view('tasks.deleted', compact('tasks'));
});

Route::post('/tasks', function () {

    $task = new App\Task;

    $task->description = request('description');

    $task->save();

    return redirect('/tasks');

});

Route::get('/tasks/complete/{id}', function($id) {

    $task = App\Task::where('id', $id)->update(['completed' => 1]);

    return redirect('/tasks');

});

Route::get('/tasks/incomplete/{id}', function($id) {
    
        $task = App\Task::where('id', $id)->update(['completed' => 0]);
    
        return redirect('/tasks');
    
});

Route::get('/tasks/delete/{id}', function($id) {
    
        $task = App\Task::where('id', $id)->update(['deleted' => 1]);
    
        return redirect('/tasks');
    
});

Route::get('/tasks/restore/{id}', function($id) {
    
        $task = App\Task::where('id', $id)->update(['deleted' => 0]);
    
        return redirect('/deleted');
    
});


